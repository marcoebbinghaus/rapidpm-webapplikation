package org.rapidpm.webapp.vaadin.ui.workingareas.stammdaten.stundensaetze.uicomponents;

/**
 * RapidPM - www.rapidpm.org
 * User: Marco Ebbinghaus
 * Date: 13.09.12
 * Time: 12:03
 * This is part of the RapidPM - www.rapidpm.org project. please contact chef@sven-ruppert.de
 */
public enum DefaultValues {
    Double("1,0"),
    Integer("1"),
    String("neu");

    private final java.lang.String defaultValue;

    private DefaultValues(final java.lang.String value) {
        this.defaultValue = value;
    }

    public java.lang.String getDefaultValue() {
        return defaultValue;
    }
}
