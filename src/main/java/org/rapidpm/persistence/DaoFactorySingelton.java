package org.rapidpm.persistence;


/**
 * RapidPM - www.rapidpm.org
 * User: svenruppert
 * Date: Nov 16, 2010
 * Time: 5:36:49 PM
 * This is part of the RapidPM - www.rapidpm.org project. please contact sven.ruppert@rapidpm.org
 */
public class DaoFactorySingelton {
    private static DaoFactory daoFactory = new DaoFactory("baseormJDBC");

    /**
     * Gets the instance.
     *
     * @return the instance
     */
    public static DaoFactory getInstance() {
        return daoFactory;
    }

    private DaoFactorySingelton() {
    }
}